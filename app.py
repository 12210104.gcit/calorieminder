from flask import Flask, redirect,  render_template, request , session, url_for 
# import pickle 
import numpy as np
import pandas as pd
import joblib
# result = 0
app  = Flask(__name__)
app.secret_key = 'your_secret_key'

veg = []
nonveg = []
cer = []
non_alc = []
alc = []
fruits = []
# load our model
model = joblib.load('model.pkl','rb')
# model = pickle.load(open("model.pkl",'rb'))
cols = ['age','weight(kg)','height(m)','gender','BMI','BMR','activity_level']

food = pd.read_excel("food.xlsx")





@app.route("/calculate", methods=['POST'])

def prediction():
    if request.method == 'POST':
        age = int(request.form['age'])
        gender = request.form['gender']
        weight = float(request.form['weight(kg)'])
        height = float(request.form['height(m)'])
        activity_level = request.form['activity']

        x_sample = [age, weight, height, gender]
        heightInMeters = x_sample[2] / 100
        bmi = x_sample[1] / (heightInMeters * heightInMeters)
        bmi = np.round(bmi, 2)

        if x_sample[3] == 'M':
            bmr = 10 * x_sample[1] + 6.25 * height - 5 * x_sample[1] + 5
        else:
            bmr = 10 * x_sample[1] + 6.25 * height - 5 * x_sample[1] - 161

        sample = pd.DataFrame([[age, weight, heightInMeters, gender, bmi, bmr, activity_level]], columns=cols)
        result = model.predict(sample)
        result = np.round(result, 2)


        if bmi < 18.5:
            category = "Underweight"
        elif 18.5 <= bmi < 25:
            category = "Normal"
        elif 25 <= bmi < 30:
            category = "Overweight"
        else:
            category = "Obese"

        # Store the results in the session
        session['result'] = result[0]

        return render_template("output.html", bmi=bmi, category=category, result=result[0])


@app.route("/recom")
def recom():
    result = session.get('result')

    # Initialize the recommendation lists
    veg_cal = result * 0.4
    ll_veg = veg_cal - 50
    ul_veg = veg_cal + 50
    sum_of_column = 0
    random_subset = pd.DataFrame()

    while not (ll_veg <= sum_of_column <= ul_veg):
        random_subset = pd.DataFrame(food[food['type'] == 'vegetable'].sample(n=3), columns=food.columns)
        sum_of_column = random_subset['calories'].sum(axis=0)

    veg = list(random_subset['foodname'].values)

    nonveg_cal = result * 0.4
    ll_nonveg = nonveg_cal - 50
    ul_nonveg = nonveg_cal + 50
    summ = 0

    while not (ll_nonveg <= summ <= ul_nonveg):
        non_veg = food[food['type'] == 'Meat'].sample(n=3)
        summ = non_veg['calories'].sum(axis=0)

    nonveg = list(non_veg['foodname'].values)
    #for cereal   
    cereal_cal = result*0.1
    ll_c = cereal_cal-50
    ul_c = cereal_cal+100
    sum_cereal = 0
    while not (ll_c <= sum_cereal <= ul_c):
        cereal = food[food['type'] == 'cereal'].sample(n=3)
        sum_cereal = cereal['calories'].sum(axis=0)

    for item in cereal['foodname'].values:
         pd.Series(cer.append(item))
    
    
        #for soft_drinks   
    drink_cal = result*0.1
    ll_d = drink_cal-50
    ul_d = drink_cal+50
    sum_drinks = 0
    while not (ll_d <= sum_drinks <= ul_d):
        drink = food[food['type'] == 'drinks'].sample(n=3)
        sum_drinks = drink['calories'].sum(axis = 0)

    for items in drink['foodname'].values:
        pd.Series(non_alc.append(items))
   
        #for alcoholic   
    acl_cal = result*0.1
    ll = acl_cal-50
    ul = acl_cal+50
    sum_alc = 0
    while not (ll <= sum_alc <= ul):
        alcohol = food[food['type'] == 'alcohol'].sample(n=3)
        sum_alc = alcohol['calories'].sum(axis = 0)


    for items in alcohol['foodname'].values:
        pd.Series(alc.append(items))
    
    
    
        #for fruit   
    fruit_cal = result*0.1
    ll = fruit_cal-50
    ul = fruit_cal+50
    sum_fruit = 0

    while not (ll <= sum_fruit <= ul):
        fruit = food[food['type'] == 'fruits'].sample(n=3)
        sum_fruit = fruit['calories'].sum(axis = 0)

        for items in fruit['foodname'].values:
            pd.Series(fruits.append(items))
   

        return render_template("recom.html", veg=veg, non_veg=nonveg,
                               cereal=cer, non_alc=non_alc, alc=alc, fruit=fruits) 
@app.route("/bmi")
def bmi():
    return render_template("bmi.html") 
@app.route("/about")
def about():
    return render_template("about.html")   
@app.route("/")
def index():
    return render_template("index.html")


if __name__ == "__main__":
    app.run(debug=True)

